{-# OPTIONS --safe --without-K #-}

-- file loaded with Agda version 2.6.0

open import Level

open import Data.Bool using (Bool)
open import Data.List using (List; []; _∷_)
open import Data.Integer using (ℤ)

data All′ {a b} {A : Set a} (P : List A → A → Set b) : List A → Set (a ⊔ b) where
  []  : All′ P []
  _∷_ : ∀ {x xs} (px : P xs x) (pxs : All′ P xs) → All′ P (x ∷ xs)

data Type : Set where
  num bool : Type

Val : Type → Set
Val num = ℤ
Val bool = Bool
