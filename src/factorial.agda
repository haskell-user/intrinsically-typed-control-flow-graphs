{-# OPTIONS --safe --without-K #-}

-- file loaded with Agda version 2.6.0

open import Data.Integer using (+_; -_)
open import Data.List using (List; _∷_; [])
open import Data.List.All using ([]; _∷_)
open import Data.List.Any using (here; there)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)

open import defs
open import while
open import flowgraph
open import translator

{-
factorial:

r = 1;
while i >= 1
  r = r * i;
  i = i + -1;

i - 0 - here refl
r - 1 - there (here refl)
-}

factorial : List (Stmt (num ∷ num ∷ []))
factorial =
  assign
    (there (here refl))
    (lit (+ 1)) ∷
  while
    (le
      (lit (+ 1))
      (var (here refl)))
    (assign
      (there (here refl))
      (mul (var (there (here refl))) (var (here refl))) ∷
    assign
      (here refl)
      (add (var (here refl)) (lit (- + 1))) ∷
    []) ∷
  []

eval-factorial-of-3 : EvalStmts (+ 3 ∷ + 0 ∷ []) factorial (+ 0 ∷ + 6 ∷ [])
eval-factorial-of-3 =
  eval-seq
    (eval-assign
      (eval-lit (+ 1))
      (there (here refl)))
    (eval-seq
      (eval-while-true
        (eval-le (eval-lit (+ 1)) (eval-var (here refl)))
        (eval-seq
          (eval-assign
            (eval-mul (eval-var (there (here refl))) (eval-var (here refl)))
            (there (here refl)))
          (eval-seq
            (eval-assign
              (eval-add (eval-var (here refl)) (eval-lit (- + 1)))
              (here refl))
            eval-skip))
       {-{!-}(eval-while-true
          (eval-le (eval-lit (+ 1)) (eval-var (here refl)))
          (eval-seq
            (eval-assign
              (eval-mul (eval-var (there (here refl))) (eval-var (here refl)))
              (there (here refl)))
            (eval-seq
              (eval-assign
                (eval-add (eval-var (here refl)) (eval-lit (- + 1)))
                (here refl))
              eval-skip))
          (eval-while-true
            (eval-le (eval-lit (+ 1)) (eval-var (here refl)))
            (eval-seq
              (eval-assign
                (eval-mul (eval-var (there (here refl))) (eval-var (here refl)))
                (there (here refl)))
              (eval-seq
                (eval-assign
                  (eval-add (eval-var (here refl)) (eval-lit (- + 1)))
                  (here refl))
                eval-skip))
            (eval-while-false
              (eval-le (eval-lit (+ 1)) (eval-var (here refl)))
              _))){-!}-})
      eval-skip)

_ : translate factorial ≡
  jf-instr (push (+ 1))
    (jf-instr (store (there (here refl)))
      (fglet
        stop
        (fgfix
          (fglet
            (jf-instr (load (there (here refl)))
              (jf-instr (load (here refl))
                (jf-instr mul
                  (jf-instr (store (there (here refl)))
                    (jf-instr (load (here refl))
                      (jf-instr (push (- + 1))
                        (jf-instr add
                          (jf-instr (store (here refl))
                            (goto (here refl))))))))))
            (jf-instr (push (+ 1))
              (jf-instr (load (here refl))
                (jf-instr le
                  (branch (here refl) (there (there (here refl)))))))))))
_ = refl

_ : EvalFlowGraph [] (+ 3 ∷ + 0 ∷ []) [] (translate factorial) (+ 0 ∷ + 6 ∷ []) []
--_ = translate-correct eval-factorial-of-3
_ =
  eval-jf-instr (eval-push [] (+ 1))
    (eval-jf-instr (eval-store [] (there (here refl)) (+ 1))
      (eval-let
        (eval-fix
          (eval-let
            (eval-jf-instr (eval-push [] (+ 1))
              (eval-jf-instr (eval-load (+ 1 ∷ []) (here refl))
                (eval-jf-instr (eval-le [] (+ 1) (+ 3))
                  (eval-branch-true
                    (eval-jf-instr (eval-load [] (there (here refl)))
                      (eval-jf-instr (eval-load (+ 1 ∷ []) (here refl))
                        (eval-jf-instr (eval-mul [] (+ 1) (+ 3))
                          (eval-jf-instr (eval-store [] (there (here refl)) (+ 3))
                            (eval-jf-instr (eval-load [] (here refl))
                              (eval-jf-instr (eval-push (+ 3 ∷ []) (- + 1))
                                (eval-jf-instr (eval-add [] (+ 3) (- + 1))
                                  (eval-jf-instr (eval-store [] (here refl) (+ 2))
                                    (eval-goto
                                      (eval-fix
                                        (eval-let
                                          (eval-jf-instr (eval-push [] (+ 1))
                                            (eval-jf-instr (eval-load (+ 1 ∷ []) (here refl))
                                              (eval-jf-instr (eval-le [] (+ 1) (+ 2))
                                                (eval-branch-true
                                                  (eval-jf-instr (eval-load [] (there (here refl)))
                                                    (eval-jf-instr (eval-load (+ 3 ∷ []) (here refl))
                                                      (eval-jf-instr (eval-mul [] (+ 3) (+ 2))
                                                        (eval-jf-instr (eval-store [] (there (here refl)) (+ 6))
                                                          (eval-jf-instr (eval-load [] (here refl))
                                                            (eval-jf-instr (eval-push (+ 2 ∷ []) (- + 1))
                                                              (eval-jf-instr (eval-add [] (+ 2) (- + 1))
                                                                (eval-jf-instr (eval-store [] (here refl) (+ 1))
                                                                  (eval-goto
                                                                    (eval-fix
                                                                      (eval-let
                                                                        (eval-jf-instr (eval-push [] (+ 1))
                                                                          (eval-jf-instr (eval-load (+ 1 ∷ []) (here refl))
                                                                            (eval-jf-instr (eval-le [] (+ 1) (+ 1))
                                                                              (eval-branch-true
                                                                                (eval-jf-instr (eval-load [] (there (here refl)))
                                                                                  (eval-jf-instr (eval-load (+ 6 ∷ []) (here refl))
                                                                                    (eval-jf-instr (eval-mul [] (+ 6) (+ 1))
                                                                                      (eval-jf-instr (eval-store [] (there (here refl)) (+ 6))
                                                                                        (eval-jf-instr (eval-load [] (here refl))
                                                                                          (eval-jf-instr (eval-push (+ 1 ∷ []) (- + 1))
                                                                                            (eval-jf-instr (eval-add [] (+ 1) (- + 1))
                                                                                              (eval-jf-instr (eval-store [] (here refl) (+ 0))
                                                                                                (eval-goto
                                                                                                  (eval-fix
                                                                                                    (eval-let
                                                                                                      (eval-jf-instr (eval-push [] (+ 1))
                                                                                                        (eval-jf-instr (eval-load (+ 1 ∷ []) (here refl))
                                                                                                          (eval-jf-instr (eval-le [] (+ 1) (+ 0))
                                                                                                            (eval-branch-false (eval-stop []))))))))))))))))))))))))))))))))))))))))))))))))))))))
