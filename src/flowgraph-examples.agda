{-# OPTIONS --safe --without-K #-}

-- file loaded with Agda version 2.6.0

open import Data.Bool using (Bool; true; false)
open import Data.Integer using (+_)
open import Data.List using ([]; _∷_)
open import Data.List.All using ([]; _∷_)
open import Data.List.Any using (here; there)
open import Relation.Binary.PropositionalEquality using (refl)
open import Relation.Nullary using (¬_)

open import defs
open import flowgraph

{-
example  1

load x
load y
store x
store y
stop

swaps the values of the two number values in the environment
-}
exmpl1 : FlowGraph (num ∷ num ∷ []) [] []
exmpl1 =
  jf-instr (load (here refl))
    (jf-instr (load (there (here refl)))
      (jf-instr (store (here refl))
        (jf-instr (store (there (here refl)))
          stop)))

exmpl1-exe : EvalFlowGraph [] (+ 2 ∷ + 3 ∷ []) [] exmpl1 (+ 3 ∷ + 2 ∷ []) []
exmpl1-exe =
  eval-jf-instr (eval-load _ _)
    (eval-jf-instr (eval-load _ _)
      (eval-jf-instr (eval-store _ _ _)
        (eval-jf-instr (eval-store _ _ _)
          (eval-stop _))))

{-
example 2

    branch l2 l1
l2: push 3
    stop
l1: stop
-}
exmpl2 : FlowGraph [] [] (bool ∷ [])
exmpl2 =
  fglet stop
    (fglet (jf-instr (push (+ 3)) stop)
      (branch (here refl) (there (here refl))))

exmpl2-exe-true : EvalFlowGraph [] [] (true ∷ []) exmpl2 [] (+ 3 ∷ [])
exmpl2-exe-true =
  eval-let
    (eval-let
      (eval-branch-true
        (eval-jf-instr (eval-push _ _)
          (eval-stop _))))

exmpl2-exe-false : EvalFlowGraph [] [] (false ∷ []) exmpl2 [] []
exmpl2-exe-false =
  eval-let
    (eval-let
      (eval-branch-false
        (eval-stop _)))

{-
example 3

l: goto l

does not terminate
-}
exmpl3 : FlowGraph [] [] []
exmpl3 =
  fgfix
    (goto (here refl))

exmpl3-exe : ∀ {stack-types} (st : Stack stack-types) →
  ¬ EvalFlowGraph [] [] [] exmpl3 [] st
exmpl3-exe st (eval-fix (eval-goto exe)) =
  exmpl3-exe st exe

{-
example 4

l: push 2
   goto l

is not expressible
-}

{-
example 5

l: push 2
   stop

is expressible and executable
-}
exmpl5 : FlowGraph [] [] []
exmpl5 =
  fgfix
    (jf-instr (push (+ 2))
      stop)

exmpl5-exe : EvalFlowGraph [] [] [] exmpl5 [] (+ 2 ∷ [])
exmpl5-exe =
  eval-fix
    (eval-jf-instr
      (eval-push _ _)
        (eval-stop _))

{-
example 6

l2: load x
    branch l2 l1
l1: stop

depending on the environment with one boolean entry:
true  - does not terminate
flase - terminates
-}
exmpl6 : FlowGraph (bool ∷ []) [] []
exmpl6 =
  fglet stop
    (fgfix
      (jf-instr (load (here refl))
        (branch (here refl) (there (here refl)))))

exmpl6-exe-true : (b : Bool) (stack-types : StackTypes) (stack : Stack stack-types) →
  ¬ EvalFlowGraph [] (true ∷ []) [] exmpl6 (b ∷ []) stack
exmpl6-exe-true b stack-types stack (eval-let exe) =
  step b stack-types stack exe
  where
    step : (b : Bool) (stack-types : StackTypes) (stack : Stack stack-types) →
      ¬ EvalFlowGraph (stop ∷ []) (true ∷ []) []
        (fgfix
          (jf-instr (load (here refl))
            (branch (here refl) (there (here refl)))))
        (b ∷ []) stack
    step b stack-types stack (eval-fix (eval-jf-instr (eval-load _ _) (eval-branch-true exe))) =
      step b stack-types stack exe

exmpl6-exe-false : EvalFlowGraph [] (false ∷ []) [] exmpl6 (false ∷ []) []
exmpl6-exe-false =
  eval-let
    (eval-fix
      (eval-jf-instr (eval-load _ _)
        (eval-branch-false
          (eval-stop _))))
