{-# OPTIONS --safe --without-K #-}

-- file loaded with Agda version 2.6.0

open import Data.Bool using (Bool; false; true)
open import Data.Integer using (ℤ; _+_; _*_; _≟_; _≤?_; +_; -_)
open import Data.List using (List; []; _∷_)
open import Data.List.All using (All; []; _∷_)
open import Data.List.Any using (here; there)
open import Data.List.Membership.Propositional using (_∈_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)
open import Relation.Nullary.Decidable using (⌊_⌋)

open import defs

-- syntax

EnvTypes = List Type
StackTypes = List Type

-- jump free instructions
data JFInstr (env-types : EnvTypes) : StackTypes → StackTypes → Set where
  load : ∀ {type stack-types} →
    type ∈ env-types →
    JFInstr env-types stack-types (type ∷ stack-types)
  store : ∀ {type stack-types} →
    type ∈ env-types →
    JFInstr env-types (type ∷ stack-types) stack-types
  push : ∀ {type stack-types} →
    Val type →
    JFInstr env-types stack-types (type ∷ stack-types)
  pop : ∀ {type stack-types} →
    JFInstr env-types (type ∷ stack-types) stack-types
  add : ∀ {stack-types : StackTypes} →
    JFInstr env-types (num ∷ num ∷ stack-types) (num ∷ stack-types)
  mul : ∀ {stack-types : StackTypes} →
    JFInstr env-types (num ∷ num ∷ stack-types) (num ∷ stack-types)
  eq : ∀ {stack-types : StackTypes} →
    JFInstr env-types (num ∷ num ∷ stack-types) (bool ∷ stack-types)
  le : ∀ {stack-types : StackTypes} →
    JFInstr env-types (num ∷ num ∷ stack-types) (bool ∷ stack-types)

data FlowGraph (env-types : EnvTypes) (labels : List StackTypes) :
  StackTypes → Set where
  stop : ∀ {stack-types} →
    FlowGraph env-types labels stack-types
  goto : ∀ {stack-types} →
    stack-types ∈ labels →
    FlowGraph env-types labels stack-types
  branch : ∀ {stack-types} →
    stack-types ∈ labels →
    stack-types ∈ labels →
    FlowGraph env-types labels (bool ∷ stack-types)
  jf-instr : ∀ {stack-types stack-types′} →
    JFInstr env-types stack-types stack-types′ →
    FlowGraph env-types labels stack-types′ →
    FlowGraph env-types labels stack-types
  fglet : ∀ {stack-types stack-types′} →
    FlowGraph env-types labels stack-types′ →
    FlowGraph  env-types (stack-types′ ∷ labels) stack-types →
    FlowGraph env-types labels stack-types
  fgfix : ∀ {stack-types} →
    FlowGraph env-types (stack-types ∷ labels) stack-types →
    FlowGraph env-types labels stack-types

-- semantics

Env : EnvTypes → Set
Env = All Val

Stack : StackTypes → Set
Stack = All Val

_ = Env (num ∷ num ∷ [])

_ = Stack (num ∷ bool ∷ [])

_ : Env (num ∷ num ∷ bool ∷ [])
_ = + 1 ∷ - + 2 ∷ false ∷ []

_ : {stack-types : StackTypes} → Stack stack-types → Stack (num ∷ stack-types)
_ = λ st → + 3 ∷ st

env-load : {env-types : EnvTypes} {type : Type} → type ∈ env-types → Env env-types → Val type
env-load (here refl) (val ∷ _) = val
env-load (there type-therein) (_ ∷ σ) = env-load type-therein σ

env-store : {env-types : EnvTypes} {type : Type} → type ∈ env-types → Env env-types → Val type → Env env-types
env-store (here refl) (_ ∷ σ) new-val = new-val ∷ σ
env-store (there type-therein) (val ∷ σ) new-val = val ∷ env-store type-therein σ new-val

data EvalJFInstr {env-types : EnvTypes} (env : Env env-types) :
  {stack-types stack-types′ : StackTypes} → Stack stack-types →
  JFInstr env-types stack-types stack-types′ →
  Env env-types → Stack stack-types′ → Set where
  eval-load : ∀ {stack-types type}
    (st : Stack stack-types) (cell : type ∈ env-types) →
    EvalJFInstr env st (load cell) env (env-load cell env ∷ st)
  eval-store : ∀ {stack-types type}
    (st : Stack stack-types) (cell : type ∈ env-types) (val : Val type) →
    EvalJFInstr env (val ∷ st) (store cell) (env-store cell env val) st
  eval-push : ∀ {stack-types type}
    (st : Stack stack-types) (val : Val type) →
    EvalJFInstr env st (push val) env (val ∷ st)
  eval-pop : ∀ {stack-types type}
    (st : Stack stack-types) (val : Val type) →
    EvalJFInstr env (val ∷ st) pop env st
  eval-add : ∀ {stack-types}
    (st : Stack stack-types) (n m : Val num) →
    EvalJFInstr env (m ∷ n ∷ st) add env (n + m ∷ st)
  eval-mul : ∀ {stack-types}
    (st : Stack stack-types) (n m : Val num) →
    EvalJFInstr env (m ∷ n ∷ st) mul env (n * m ∷ st)
  eval-eq : ∀ {stack-types} →
    (st : Stack stack-types) (n m : Val num) →
    EvalJFInstr env (m ∷ n ∷ st) eq env (⌊ n ≟ m ⌋ ∷ st)
  eval-le : ∀ {stack-types} →
    (st : Stack stack-types) (n m : Val num) →
    EvalJFInstr env (m ∷ n ∷ st) le env (⌊ n ≤? m ⌋ ∷ st)

flowgraphs-for-labels : EnvTypes → List StackTypes → Set
flowgraphs-for-labels env-types = All′ (FlowGraph env-types)

rest-labels :
  {labels : List StackTypes} {l : StackTypes} → l ∈ labels → List StackTypes
rest-labels {_ ∷ labels} (here refl) = labels
rest-labels (there in-labels) = rest-labels in-labels

access-fg :
  {env-types : EnvTypes} {labels : List StackTypes} →
  flowgraphs-for-labels env-types labels →
  {l : StackTypes} (l-in-labels : l ∈ labels) →
  FlowGraph env-types (rest-labels l-in-labels) l
access-fg (fg ∷ _) (here refl) = fg
access-fg (_ ∷ fgs) (there therein) = access-fg fgs therein

rest-fgs :
  {env-types : EnvTypes}
  {labels : List StackTypes} →
  flowgraphs-for-labels env-types labels →
  {l : StackTypes} (l-in-labels : l ∈ labels) →
  flowgraphs-for-labels env-types (rest-labels l-in-labels)
rest-fgs (_ ∷ fgs) (here refl) = fgs
rest-fgs (_ ∷ fgs) (there l-therein) = rest-fgs fgs l-therein

data EvalFlowGraph {labels : List StackTypes} {env-types : EnvTypes}
  (fgs : flowgraphs-for-labels env-types labels) (env : Env env-types) :
  {stack-types : StackTypes} → Stack stack-types →
  FlowGraph env-types labels stack-types → Env env-types →
  {stack-types′ : StackTypes} → Stack stack-types′ → Set where
  eval-stop : ∀ {stack-types} (st : Stack stack-types) →
    EvalFlowGraph fgs env st stop env st
  eval-goto : ∀ {stack-types stack-types′ env′ l}
    {st : Stack stack-types} {st′ : Stack stack-types′} →
    EvalFlowGraph (rest-fgs fgs l) env st (access-fg fgs l) env′ st′ →
    EvalFlowGraph fgs env st (goto l) env′ st′
  eval-branch-true : ∀ {stack-types stack-types′ env′ l l′}
    {st : Stack stack-types} {st′ : Stack stack-types′} →
    EvalFlowGraph (rest-fgs fgs l) env st (access-fg fgs l) env′ st′ →
    EvalFlowGraph fgs env (true ∷ st) (branch l l′) env′ st′
  eval-branch-false : ∀ {stack-types stack-types′ env′ l l′}
    {st : Stack stack-types} {st′ : Stack stack-types′} →
    EvalFlowGraph (rest-fgs fgs l′) env st (access-fg fgs l′) env′ st′ →
    EvalFlowGraph fgs env (false ∷ st) (branch l l′) env′ st′
  eval-jf-instr : ∀ {stack-types stack-types′ stack-types″ env′ env″}
    {instr : JFInstr env-types stack-types stack-types′}
    {fg : FlowGraph env-types labels stack-types′}
    {st : Stack stack-types} {st′ : Stack stack-types′} {st″ : Stack stack-types″} →
    EvalJFInstr env st instr env′ st′ →
    EvalFlowGraph fgs env′ st′ fg env″ st″ →
    EvalFlowGraph fgs env st (jf-instr instr fg) env″ st″
  eval-let : ∀ {stack-types stack-types′ stack-types″ env″}
    {fg : FlowGraph env-types labels stack-types′}
    {fg′ : FlowGraph env-types (stack-types′ ∷ labels) stack-types}
    {st : Stack stack-types} {st″ : Stack stack-types″} →
    EvalFlowGraph (fg ∷ fgs) env st fg′ env″ st″ →
    EvalFlowGraph fgs env st (fglet fg fg′) env″ st″
  eval-fix : ∀ {stack-types stack-types′ env′}
    {fg : FlowGraph env-types (stack-types ∷ labels) stack-types}
    {st : Stack stack-types} {st′ : Stack stack-types′} →
    EvalFlowGraph (fgfix fg ∷ fgs) env st fg env′ st′ →
    EvalFlowGraph fgs env st (fgfix fg) env′ st′
